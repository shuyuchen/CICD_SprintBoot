# 使用基於 OpenJDK 17 的 JRE 映像作為運行環境
FROM openjdk:8

# 設置工作目錄
WORKDIR /app

# 從構建環境中複製構建生成的 JAR 文件到容器
COPY app.jar ./app.jar

# 指定容器對外暴露的埠號
EXPOSE 8080

# 設置容器啟動命令
CMD ["java", "-jar", "app.jar"]