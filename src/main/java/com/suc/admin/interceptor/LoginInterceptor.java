package com.suc.admin.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
* 登入檢查
* 1. 配置好攔截器要攔截那些請求
* 2. 把這些配置放在容器中
* */
@Slf4j
public class LoginInterceptor implements HandlerInterceptor {

    /*
    * 目標方法執行以後
    * */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        log.info("preHandle攔截請求路徑是{}",requestURI);

        // 登入檢查邏輯
        HttpSession session = request.getSession();

        Object loginUser = session.getAttribute("loginUser");
        if(loginUser != null){

            return true; //放行
        }
        request.setAttribute("msg","請先登入!!!");
        request.getRequestDispatcher("/").forward(request,response);

        return false; //攔截
    }

    /*
     * 目標方法執行完成以後
     * */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

        log.info("postHandle執行{}",modelAndView);
    }

    /*
     * 頁面渲染以後
     * */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

        log.info("afterCompletion執行異常{}",ex);
    }
}
