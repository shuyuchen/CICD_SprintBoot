package com.suc.admin.config;

import com.suc.admin.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/*
* 1. 編寫一個攔截器實現HandlerInterceptor接口
* 2. 攔截器註冊到容器中(覆寫WebMvcConfigurer的addInterceptors)
* 3. 指定攔截規則(如果是攔截所有,靜態資源也會被攔截)
* */
@Configuration
public class AdminWebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/","/login","/css/**","/fonts/**","/images/**","/js/**");

    }
}
