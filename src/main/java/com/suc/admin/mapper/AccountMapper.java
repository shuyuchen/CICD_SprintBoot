package com.suc.admin.mapper;

import com.suc.admin.bean.Account;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AccountMapper {

    public Account getAcct(Long id);
}
