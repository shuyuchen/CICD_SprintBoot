package com.suc.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suc.admin.bean.User;
/*
* 繼承BaseMapper就會有簡單的增刪改查功能
* */
public interface UserMapper extends BaseMapper<User> {
}
