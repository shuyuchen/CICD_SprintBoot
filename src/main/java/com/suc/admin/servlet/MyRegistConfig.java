package com.suc.admin.servlet;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class MyRegistConfig {

    @Bean
    public ServletRegistrationBean myServlet(){
        MyServlet myServlet = new MyServlet();
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(myServlet);
        servletRegistrationBean.setUrlMappings(Arrays.asList("/my", "/my02"));

        return servletRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean myFilter(){

        MyFliter myFliter=new MyFliter();
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(myFliter);
        filterRegistrationBean.setUrlPatterns(Arrays.asList("/my", "/css/*"));
        return filterRegistrationBean;
    }

    @Bean
    public ServletListenerRegistrationBean myListener(){
        MyServletContextListener myServletContextListener = new MyServletContextListener();
        return new ServletListenerRegistrationBean(myServletContextListener);
    }
}
