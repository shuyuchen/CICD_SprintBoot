package com.suc.admin.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("user_tbl")
public class User {

    /*
    * 所有屬性都應該在數據庫中
    * 若沒有需要用@TableField(exist = false)
    * */
    @TableField(exist = false)
    private String userName;
    @TableField(exist = false)
    private String passWord;

// 以下是數據庫資料
    private Long id;
    private String name;
    private Integer age;
    private String email;
}
