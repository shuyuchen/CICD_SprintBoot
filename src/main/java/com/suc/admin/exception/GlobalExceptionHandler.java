package com.suc.admin.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/*
*  處理整個web controller的異常
* */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    //處理數學運算異常
    @ExceptionHandler({ArithmeticException.class, NullPointerException.class})
    public String hanflerArithException(Exception e){

        log.error("異常是:{}",e);
        return "login";  //視圖地址
    }
}
