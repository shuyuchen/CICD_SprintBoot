package com.suc.admin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Slf4j
@Controller
public class FormTestController {

    @GetMapping("/form_layouts")
    public String form_layouts() {
        return "form/form_layouts";
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("email") String email,
                         @RequestParam("username") String username,
                         @RequestParam("headerImg") MultipartFile headerImg,
                         @RequestParam("photos") MultipartFile[] photos) throws IOException {
        log.info("上傳的訊息：email={}, username={}, headerImg={}, photos={}"
                , email, username, headerImg.getSize(), photos.length);

        String savePath = "D:\\桌面\\Spring Boot_workspace\\uploadImage\\" + username + "\\";
        if (!headerImg.isEmpty()) {
            // 保存到文件伺服器
            String originalFilename = headerImg.getOriginalFilename();

            makePath(savePath + "headerImg\\");

            headerImg.transferTo(new File(savePath + "headerImg\\" + originalFilename));
        }

        if (photos.length > 0) {
            for (MultipartFile photo : photos) {
                if (!photo.isEmpty()) {
                    // 保存到文件伺服器
                    String photoFilename = photo.getOriginalFilename();
                    makePath(savePath + "photos\\");
                    photo.transferTo(new File(savePath + "photos\\" + photoFilename));
                }
            }
        }

        return "main";
    }

    private void makePath(String path) {
        File filePath = new File(path);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
    }
}
