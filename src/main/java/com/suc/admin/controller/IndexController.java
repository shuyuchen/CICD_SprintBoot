package com.suc.admin.controller;

import com.suc.admin.bean.Account;
import com.suc.admin.bean.City;
import com.suc.admin.bean.User;
import com.suc.admin.service.AccountService;
import com.suc.admin.service.CityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Slf4j
@Controller
public class IndexController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    AccountService accountService;

    @Autowired
    CityService cityService;

    @ResponseBody
    @PostMapping("/city")
    public City saveCity(City city){
        cityService.saveCity(city);
        return city;
    }


    @ResponseBody
    @GetMapping("/city")
    public City getCityById(@RequestParam("id") Long id){

        return cityService.getById(id);
    }

    @ResponseBody
    @GetMapping("/acct")
    public Account getById(@RequestParam("id") Long id){

        return accountService.getAcctById(id);
    }

    /*
    * 來到登入頁
    * */
    @GetMapping(value = {"/","/login"})
    public String loginPage(){
        return "login";
    }

    /*
     * 登入頁--登入用
     * */
    @PostMapping("/login")
    public String main(User user, HttpSession session, Model model){

        if(!user.getUserName().isEmpty() && !user.getPassWord().isEmpty()){
            // 將登入成功的用戶保存起來
            session.setAttribute("loginUser",user);
            // 登入成功後，重定向到main.html
            return "redirect:/main.html";
        }else {
            model.addAttribute("msg", "帳號密碼錯誤");
            //回到登入頁面
            return "login";
        }
    }

    /*
    * 來到main頁面
    * */
    @GetMapping("/main.html")
    public String mainPage(HttpSession session, Model model){

        log.info("當前方法是:{}","mainPage");
        // 是否登入。 攔截器、過濾器
//        Object loginUser = session.getAttribute("loginUser");
//        if(loginUser != null){
//            return "main";
//        }else {
//            model.addAttribute("msg", "請重新登入");
//            //回到登入頁面
//            return "login";
//        }

        return "main";
    }
}
