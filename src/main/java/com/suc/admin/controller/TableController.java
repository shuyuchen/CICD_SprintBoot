package com.suc.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suc.admin.bean.User;
import com.suc.admin.exception.UserTooManyException;
import com.suc.admin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.websocket.server.PathParam;
import java.util.Arrays;
import java.util.List;

@Controller
public class TableController {

    @Autowired
    UserService userService;



    /*
    *  不帶請求參數或者參數類型不對  400;Bad Request 一般都是瀏覽器的參數沒有傳遞正確
    * */
    @GetMapping("/basicTable")
    public String basicTable(@RequestParam("a") int a){
//        int i = 10/0;
        return "table/basic_table";
    }

    @GetMapping("/user/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id,
                             @RequestParam(value = "pn", defaultValue = "1") Integer pn,
                             RedirectAttributes ra){
        userService.removeById(id);
        ra.addAttribute("pn",pn);

        return "redirect:/dynamic_table";
    }

    @GetMapping("/dynamic_table")
    public String dynamicTable(@RequestParam(value = "pn", defaultValue = "1") Integer pn, Model model){
        //表格內容的遍歷
//        List<User> users = Arrays.asList(
//                new User("aaa","123"),
//                new User("bbb","234"),
//                new User("ccc","345"),
//                new User("ddd","456")
//                );
//        model.addAttribute("users",users);

//        if(users.size()>3){
//            throw new UserTooManyException();
//        }
//        List<User> userList = userService.list();
//        model.addAttribute("users", userList);

        // 分頁查詢數據
        Page<User> userPage = new Page<>(pn, 2);

        // 分頁查詢的結果 (null為查詢條件)
        Page<User> page = userService.page(userPage, null);
        long current = page.getCurrent();
        long pages = page.getPages();
        long total = page.getTotal();
        List<User> records = page.getRecords();
        model.addAttribute("users", page);
        return "table/dynamic_table";
    }

    @GetMapping("/responsiveTable")
    public String responsiveTable(){
        return "table/responsive_table";
    }

    @GetMapping("/editTable")
    public String editTable(){
        return "table/editable_table";
    }
}
