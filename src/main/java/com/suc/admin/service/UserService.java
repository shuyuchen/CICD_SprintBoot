package com.suc.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suc.admin.bean.User;

public interface UserService extends IService<User> {
}
