package com.suc.admin.service;

import com.suc.admin.bean.Account;

public interface AccountService {

    public Account getAcctById(Long id);

}
