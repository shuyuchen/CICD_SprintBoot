package com.suc.admin.service.impl;

import com.suc.admin.bean.Account;
import com.suc.admin.mapper.AccountMapper;
import com.suc.admin.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    public Account getAcctById(Long id){
        return accountMapper.getAcct(id);
    }
}
