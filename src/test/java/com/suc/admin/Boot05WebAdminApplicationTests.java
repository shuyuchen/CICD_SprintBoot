package com.suc.admin;

import com.suc.admin.bean.User;
import com.suc.admin.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.jws.soap.SOAPBinding;


@Slf4j
@SpringBootTest
class Boot05WebAdminApplicationTests {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	UserMapper userMapper;

	@Test
	void contextLoads() {
//		jdbcTemplate.queryForObject("select * from exchange_rate limit 100");
//		jdbcTemplate.queryForList("select * from exchange_rate limit 100");
		Long total = jdbcTemplate.queryForObject("select count(*) from exchange_rate ", Long.class );
		log.info("數據總數: {}",total);
	}

	@Test
	void testUserMapper(){
		User user= userMapper.selectById(1L);
		log.info("用戶訊息：{}", user);
	}
}
